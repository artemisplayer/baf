baf
===

## Presentation

**baf** allows the user to identify and localize copies of the same file across different storages or folders. The application is using a [Pyramid](https://trypyramid.com/) frontend and an [alembic](https://alembic.sqlalchemy.org/en/latest/) backend.

## Code organization

```
baf
├── alembic/versions/...        The database structure versionning
├── models                      Classes for filesystem objects representation
│   └── meta.py
├── scripts/baf_index.py        The actual search script
├── static
├── templates                   Jinja2 webpages templates
└── views                       Pyramid's interface to the database
```

## Getting Started

Go to your project's directory.
Create a Python virtual environment, and upgrade packaging tools:

```sh
python3 -m venv env
env/bin/pip install --upgrade pip setuptools
```

Install the project in editable mode with its testing requirements:

```sh
env/bin/pip install -e ".[testing]"
```

Upgrade the database to the current version using Alembic:

```sh
env/bin/alembic -c development.ini upgrade head
```

Run your project:

```sh
env/bin/pserve development.ini
```

The application should be running locally:
[http://localhost:6543](http://localhost:6543)


## Run tests

You can run your project's tests:

```sh
env/bin/pytest
```